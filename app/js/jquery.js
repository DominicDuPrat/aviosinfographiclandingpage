// Menu Highlight
    
function smoothScroll() {
    var headerHeight = $('.header').outerHeight();
    if($(window).width() > 767) {
        $('.internalLink').click(function(event){
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 500);
            event.preventDefault();
        });
    } else {
        $('.internalLink').click(function(event){
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top - headerHeight
            }, 500);
            event.preventDefault();
        });
    }
}

// Hamburger Animation control

function navBarAnimation() {
    $('#nav-icon').click(function(){
        $('.header').toggleClass('open');
        $(this).toggleClass('open');
        $('.slideDownNav').slideToggle();
    });
}

//Parallax Scrolling

function runStallar() {
    $(window).stellar({
        horizontalScrolling:false,
        hideDistantElements: false,
        scrollProperty: 'scroll',
        positionProperty: 'transform',
        verticalOffset: 0,
        horizontalOffset: 0
    });
}


$(document).ready(function(){
    smoothScroll()
    navBarAnimation()
    if ($(window).width() > 1024) {
        runStallar();
    } else {
        
    }
});

   


